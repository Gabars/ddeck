/*
    This file is part of Card Deck lib.

    Card Deck lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Card Deck lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Card Deck lib.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef C_DECK_H
#define C_DECK_H
#include <stdexcept>
//!Place "x.h" includes at the end


namespace dexc
{
    enum category{INFO,WARNING,CRITICAL};
    class exception : public virtual std::runtime_error
    {
    private:
        int code;
        category cat;
        //std::string msg;
    public:
        exception(int code, category cat=INFO, std::string msg=""): runtime_error(msg)
        {
            this->code=code;
            this->cat=cat;
            //this->msg=msg;

        }
        std::string get_catStr()
        {
            switch(cat)
            {
                case INFO:
                    return "INFO";
                    break;
                case WARNING:
                    return "WARNING";
                    break;
                case CRITICAL:
                    return "CRITICAL";
                    break;
                default:
                    return "UNKNOWN";
                    break;
            }
        }
        int get_code()
        {
            return code;
        }
        std::string fwhat()
        {
            struct tm * timeinfo;
            time_t rawtime;
            char time_buffer [80];
            time (&rawtime);
            timeinfo = localtime (&rawtime);
            timeinfo = localtime (&rawtime);
            strftime (time_buffer,80,"[%R] ",timeinfo);
            return time_buffer+get_catStr()+": "+what();
        }
        category get_cat()
        {
            return cat;
        }
    };
};
#include    "deck.h"
#include    "cards.h"
#endif
