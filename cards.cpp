/*
    This file is part of Card Deck lib.

    Card Deck lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Card Deck lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Card Deck lib.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include "Deck.h"

void pass_cards(unsigned int c,std::vector<card*> *to,std::vector<card*> *from) throw(dexc::exception)
{
    if(from->size()<c)
    {
        std::string msg="Not enough cards.";
        throw dexc::exception(1,dexc::INFO,msg);
    }
    for(unsigned int i=0;i!=c;i++)
    {
        to->push_back(from->back());
        from->pop_back();
    }
    return;
}

card::card()
{

}
card::card(unsigned int suit, unsigned int rank, deck *parent)
{
    init(suit,rank,parent);
}
card::~card()
{
    //Nothing to do...(yet)
}
void card::init(unsigned int suit, unsigned int rank, deck *parent)
{
    this->parent=parent;
    s=suit;
    r=rank;
}
unsigned int card::suit()
{
    return s;
}
unsigned int card::rank()
{
    return r;
}
std::string card::ssuit()
{
    return parent->suit(this);
}
std::string card::srank()
{
    return parent->rank(this);
}
