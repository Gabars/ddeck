deck=deck.cpp
cards=cards.cpp
pile=pile.cpp
hand=hand.cpp

all:
	make Debug Release Debug-static Release-static
Debug:
	echo "\nCompiling $@"
	g++ -Wall -fPIC -O0 -c -g -std=c++11 -c $(deck) -o ./obj/deck_d.o;
	g++ -Wall -fPIC -O0 -c -g -std=c++11 -c $(cards) -o ./obj/cards_d.o;
	echo "Linking $@"
	g++ -g -fPIC -shared -Wl,-soname,libdeckd.so -o ./bin/libdeckd.so ./obj/deck_d.o ./obj/cards_d.o -lc
Release:
	echo "\nCompiling $@"
	g++ -Wall -fPIC -c  -std=c++11  $(deck) -o ./obj/deck.o;
	g++ -Wall -fPIC -c  -std=c++11  $(cards) -o ./obj/cards.o;
	echo "Linking $@"
	g++ -fPIC -shared -Wl,-soname,libdeck.so -o ./bin/libdeck.so ./obj/deck.o ./obj/cards.o -lc

Debug-static:
	echo "\nCompiling $@"
	g++ -Wall -fPIC -c -g -std=c++11 $(deck) -o ./obj/deck_d.o;
	g++ -Wall -fPIC -c -g -std=c++11 $(cards) -o ./obj/cards_d.o;
	echo "Linking $@"
	ar rvs ./bin/deck_d.a ./obj/deck_d.o;
Release-static:
	echo "\nCompiling $@"
	g++ -O2 -std=c++11 -c $(deck) -o ./obj/deck.o ;
	g++ -O2 -std=c++11 -c $(cards) -o ./obj/deck.o ;
	echo "Linking $@"
	ar rvs ./bin/deck.a ./obj/deck.o;

clean:
	rm ./obj/*.o


