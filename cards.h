/*
    This file is part of Card Deck lib.

    Card Deck lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Card Deck lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Card Deck lib.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CARDS_H
#define CARDS_H
#include <string>
#include "Deck.h"

class deck;
class card;
#ifdef __cplusplus
extern "C"
{
#endif

void pass_cards(unsigned int c,std::vector<card*> *to,std::vector<card*> *from) throw(dexc::exception);

class card
{
private:
    unsigned int s,r;
    deck *parent;
public:
    card();
    void init(unsigned int suit, unsigned int rank,deck *parent);
    card(unsigned int suit,unsigned rank, deck *parent);
    unsigned int suit();
    unsigned int rank();
    std::string ssuit();
    std::string srank();
    ~card();
};

#ifdef __cplusplus
}
#endif
#endif
