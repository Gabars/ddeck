/*
    This file is part of Card Deck lib.

    Card Deck lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Card Deck lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Card Deck lib.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DECK_H
#define DECK_H

#include <string>
#include <vector>
#include <memory>


#ifdef __cplusplus
extern "C"
{
#endif



class deck;
class card;
void shuffle_pile(std::vector<card*>* pile);
class deck
{
private:
    deck(const deck &d); //Copy not allowed for now.
    //Variables:
    std::vector<std::string> suits;
    std::vector<std::string> ranks;
    std::vector<std::string> wildcards;
    std::vector<card*> cards; //Can access card with id=(it's suit*ranks)+wildcards

    unsigned int cardsn;
    bool is_init;


    //Functions:
    void parse_csv(std::vector<std::string> dump)throw(dexc::exception);
    void fill_cards();
    std::vector<std::string> read_csv(std::string fn) throw(dexc::exception);


public:
    deck();
    deck(std::vector<std::string> suits,std::vector<std::string> ranks,std::vector<std::string> wildcards= std::vector<std::string>());
    deck(std::string filename) throw(dexc::exception);
    void init(std::vector<std::string> suits,std::vector<std::string> ranks,std::vector<std::string> wildcards= std::vector<std::string>());
    void init(std::string filename) throw(dexc::exception);
    ~deck();
    card* get_card(unsigned int s,unsigned int r)throw(dexc::exception);
    card* get_card_direct(unsigned int index)throw(dexc::exception);
    std::string suit(card* c)throw(dexc::exception);;
    std::string rank(card* c)throw(dexc::exception);;
    std::vector<card*> make_pile();
    unsigned int how_many_cards();

};


#ifdef __cplusplus
}
#endif
#endif
