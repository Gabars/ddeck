/*
    This file is part of Card Deck lib.

    Card Deck lib is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Card Deck lib is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Card Deck lib.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <algorithm>    //std::random_shuffle
#include <ctime>        //std::time
#include "Deck.h"


void shuffle_pile(std::vector<card*>* pile)
{
    std::srand(std::time(0));
    std::random_shuffle(pile->begin(),pile->end());
    return;
}

deck::deck()
{
    is_init=false;
}
deck::deck(std::string filename)throw(dexc::exception)
{
    try{
    this->init(filename);}
    catch(dexc::exception e)
    {
        throw e;
    }
}
deck::deck(std::vector<std::string> s,std::vector<std::string> r,std::vector<std::string> w)
{
    this->init(s,r,w);
}

void deck::init(std::vector<std::string> s,std::vector<std::string> r,std::vector<std::string> w)
{
    //!Constructor
    suits=s;
    ranks=r;
    wildcards=w;
    fill_cards();
    is_init=true;
    return;
}
void deck::init(std::string filename) throw(dexc::exception)
{
    is_init=true;
    //!Constructor
    try{
    parse_csv(read_csv(filename));
    fill_cards();
    }
    catch(dexc::exception e)
    {
        is_init=false;
        throw e;
    }
    return;

}
deck::~deck()
{
    //!Deconstructor
    //delete cards
    for (std::vector<card*>::iterator it = cards.begin() ; it != cards.end(); it++)
    {
        delete (*it);
    }
    cards.clear();
}
std::vector<std::string> deck::read_csv(std::string fn) throw(dexc::exception)
{

    std::ifstream in(fn);
    //read all values at once -> tmp
    if(!in.is_open())
    {
        std::string msg="Could not open "+fn+" for parsing.";
        throw dexc::exception(101,dexc::WARNING,msg);
    }
    std::string line;
    std::vector<std::string> tmp;
    std::getline(in,line);

    std::stringstream lineStream(line);
    std::string cell;

    while(std::getline(lineStream,cell,','))
    {
        tmp.push_back(cell);
    }
    //done reading values
    in.close();
    return tmp;
}
void deck::parse_csv(std::vector<std::string> dump)throw(dexc::exception)
{
    enum active{SUITS,RANKS,WILD};
    active act;
    if(dump.size()==0)
    {

        throw(dexc::exception(102,dexc::WARNING,"File is empty or could not be read."));
        return;
    }
    suits.push_back("Wildcards"); //!suits[0] is always "Wildcards"
    for(unsigned int i=0; i<dump.size();i++)
    {

        //read dump string by string and copy them in their appropriate vectors.
        if(!dump[i].compare("SUITS"))
        {
            act=SUITS;
        }
        else if(!dump[i].compare("RANKS"))
        {
            act=RANKS;
        }
        else if(!dump[i].compare("WILD"))
        {
            act=WILD;
        }
        else
        {
            if(act==SUITS)
            {
                suits.push_back(dump[i]);
            }
            else if (act==RANKS)
            {
                ranks.push_back(dump[i]);
            }
            else if(act==WILD)
            {
                wildcards.push_back(dump[i]);
            }
            else
            {
                throw(dexc::exception(202,dexc::CRITICAL,"File is corrupted or invalid."));
                return;
            }

        }
    }
    if(suits.size()==0||ranks.size()==0)
    {
    //Suits and ranks are required...
        throw(dexc::exception(202,dexc::CRITICAL,"File is corrupted or invalid."));
    }
    return;
}
void deck::fill_cards()
{

    const unsigned int nranks=ranks.size();
    const unsigned int nsuits=suits.size();
    const unsigned int nwilds=wildcards.size();
    const unsigned int n=(nranks*(nsuits-1))+nwilds-1; //Because array starts at 0
    unsigned int tmp=0;
    //cards =new card[n];
    cardsn=n;

    for(unsigned int i=0;i<nwilds;i++)
    {
        cards.push_back(new card(0,i,this));
    }
    for(unsigned int s=1;s<nsuits;s++)
    {
        for(unsigned int r=0;r<nranks;r++)
        {
            tmp=((s-1)*nranks); //Had to split it for it to work properly?
            tmp+=(nwilds)+r;//Can access card with id=(it's suit*ranks)+wildcards+ it's rank
            cards.push_back(new card(s,r,this));
        }

    }
    return;
}
card* deck::get_card(unsigned int s,unsigned int r)throw(dexc::exception)
{
    if(s==0)
    {
        if(r<this->wildcards.size())
        {
            //OK
        }
        else
        {
            //Invalid card
            std::string msg="Trying to access a card that doesn't exist.";
            throw(dexc::exception(201,dexc::CRITICAL,msg));
            //card* nul=NULL;
            return nullptr;

        }
    }
    unsigned int tmp;
    //tmp=((s-1)*ranks.size());
    tmp=s*ranks.size()+r;
    if(s!=0) //Only add if card isn't suit 0 (wildcards)
        tmp+=(wildcards.size());
    return cards[tmp];

}
card* deck::get_card_direct(unsigned int index)throw(dexc::exception)
{
    return cards[index];
}
std::string deck::suit(card* c)throw(dexc::exception)
{
    if (c->suit() >suits.size())
    {
        throw(dexc::exception(103,dexc::category::WARNING,"Invalid card. (Suit)"));
        return suits[0];
    }
    std::string tmpsuit;
    if(c->suit()==0)
    tmpsuit="";//return empty string for empty
    else
    tmpsuit=suits[c->suit()];
    return tmpsuit;
}
std::string deck::rank(card* c)throw(dexc::exception)
{
    if (c->rank()>ranks.size())
    {
        throw(dexc::exception(103,dexc::category::WARNING,"Invalid card. (Rank)"));
        return ranks[0];
    };
    std::string tmpsuit;
    tmpsuit=suits[c->suit()];
    std::string tmprank;
    if(c->suit()==0)
    tmprank=wildcards[c->rank()];
    else
    tmprank=ranks[c->rank()];
    //return this->rank(c->rank());
    return tmprank;
}

std::vector<card*> deck::make_pile() //return a copy
{
    return cards;
}
unsigned int deck::how_many_cards()
{
    return cardsn;
}
