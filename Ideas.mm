<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1428077834789" ID="ID_1617864100" MODIFIED="1428122651847" STYLE="fork" TEXT="Class:Deck">
<node CREATED="1428077972002" HGAP="23" ID="ID_824676364" MODIFIED="1428122651847" POSITION="right" TEXT="Class: Pile" VSHIFT="25">
<icon BUILTIN="help"/>
<node CREATED="1428077999262" HGAP="50" ID="ID_771538739" MODIFIED="1428122651847" TEXT="Class: Hand (child of Pile)" VSHIFT="-27">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428078258816" HGAP="30" ID="ID_1529331657" MODIFIED="1428122651847" TEXT="Has less functions than Hand (smaller?)" VSHIFT="34"/>
<node CREATED="1428122480977" HGAP="32" ID="ID_1401557651" MODIFIED="1428122651847" TEXT="Receives pointers to cards" VSHIFT="-1"/>
</node>
<node CREATED="1428078049900" HGAP="46" ID="ID_675146805" MODIFIED="1428122651847" POSITION="left" TEXT="Class: Card" VSHIFT="9">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1428078115877" HGAP="-15" ID="ID_1112521775" MODIFIED="1428122651847" POSITION="right" TEXT="Contains public pointers to all Cards" VSHIFT="94"/>
<node CREATED="1428078172031" HGAP="-12" ID="ID_419614908" MODIFIED="1428122651847" POSITION="right" TEXT="Contains Cards (private)" VSHIFT="-9"/>
<node CREATED="1428078391034" HGAP="32" ID="ID_486835785" MODIFIED="1428122651847" POSITION="left" TEXT="Can save/load deck&apos;s settings" VSHIFT="57"/>
<node CREATED="1428078423483" ID="ID_1533150036" MODIFIED="1428122651847" POSITION="left" TEXT="Can save everything?">
<icon BUILTIN="clanbomber"/>
</node>
</node>
</map>
